package me.keo1711.localeprint;

import java.io.*;
import java.util.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class LocalePrintPlugin extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        getLogger().info("LOCALE2|" +
                         event.getPlayer().getDisplayName() + "/" + event.getPlayer().getUniqueId() + " " + event.getPlayer().getLocale());
    }
}
